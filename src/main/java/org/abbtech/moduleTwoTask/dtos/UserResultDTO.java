package org.abbtech.moduleTwoTask.dtos;

public record UserResultDTO(int id,String username) { }
