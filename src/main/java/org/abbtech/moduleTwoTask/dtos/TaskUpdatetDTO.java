package org.abbtech.moduleTwoTask.dtos;

import org.abbtech.moduleTwoTask.enums.TaskStatus;

import java.sql.Timestamp;

public record TaskUpdatetDTO(int id, int user_id,String title, String desc, TaskStatus status) { }
