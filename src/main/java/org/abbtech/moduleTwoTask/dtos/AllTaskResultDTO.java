package org.abbtech.moduleTwoTask.dtos;

import org.abbtech.moduleTwoTask.enums.TaskStatus;

import java.sql.Timestamp;

public record AllTaskResultDTO(int id, int user_id, String title, String desc, Timestamp created_at, Timestamp updated_at, TaskStatus status) { }
