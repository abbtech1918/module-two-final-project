package org.abbtech.moduleTwoTask.repositores;

import org.abbtech.moduleTwoTask.JDBCConnection;
import org.abbtech.moduleTwoTask.dtos.UserResultDTO;
import org.abbtech.moduleTwoTask.interfaces.IUserRepository;
import org.abbtech.moduleTwoTask.models.User;
import org.abbtech.moduleTwoTask.utils.SecurityUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserRepository implements IUserRepository {

    @Override
    public boolean checkUserExistOrNot(String username, String password) throws SQLException {
        Connection connection = null;
        connection = JDBCConnection.getConnection();
        List<UserResultDTO> users = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM users 
                WHERE username = ? and password = ?
                LIMIT 1;
                """;

        if (Objects.isNull(connection)) {
            throw new RuntimeException("SQL CONNECTION PROBLEM");
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlSelect);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, SecurityUtil.generateHash(password));
        ResultSet userResults = preparedStatement.executeQuery();
        while (userResults.next()) {
            int id = userResults.getInt("id");
            users.add(new UserResultDTO(id, username));
        }
        return !users.isEmpty();
    }
    public UserResultDTO getOneUser(String username, String password) throws SQLException {
        Connection connection = null;
        connection = JDBCConnection.getConnection();
        List<UserResultDTO> users = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM users 
                WHERE username = ? and password = ?
                LIMIT 1;
                """;

        if (Objects.isNull(connection)) {
            throw new RuntimeException("SQL CONNECTION PROBLEM");
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlSelect);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        preparedStatement.setString(1, username);
        preparedStatement.setString(2, SecurityUtil.generateHash(password));
        ResultSet userResults = preparedStatement.executeQuery();
        while (userResults.next()) {
            int id = userResults.getInt("id");
            users.add(new UserResultDTO(id, username));
        }
        return users.getFirst();
    }
}
