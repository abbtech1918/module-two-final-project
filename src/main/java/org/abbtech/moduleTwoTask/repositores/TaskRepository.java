package org.abbtech.moduleTwoTask.repositores;

import org.abbtech.moduleTwoTask.JDBCConnection;
import org.abbtech.moduleTwoTask.dtos.AllTaskResultDTO;
import org.abbtech.moduleTwoTask.dtos.TaskResultDTO;
import org.abbtech.moduleTwoTask.dtos.TaskUpdatetDTO;
import org.abbtech.moduleTwoTask.dtos.UserResultDTO;
import org.abbtech.moduleTwoTask.enums.TaskStatus;
import org.abbtech.moduleTwoTask.interfaces.ITaskRepository;
import org.abbtech.moduleTwoTask.utils.SecurityUtil;

import java.sql.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskRepository implements ITaskRepository {

    @Override
    public List<AllTaskResultDTO> getUserTasks(int user_id) throws SQLException {
        Connection connection = null;
        connection = JDBCConnection.getConnection();
        List<AllTaskResultDTO> tasks = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM tasks
                WHERE user_id = ?;
                """;
        if (Objects.isNull(connection)) {
            throw new RuntimeException("SQL CONNECTION PROBLEM");
        }
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sqlSelect);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        preparedStatement.setInt(1, user_id);
        ResultSet taskResult = preparedStatement.executeQuery();
        while (taskResult.next()) {
            int id = taskResult.getInt("id");
            String title = taskResult.getString("title");
            String desc = taskResult.getString("desc");
            Timestamp created_at = taskResult.getTimestamp("created_at");
            Timestamp updated_at = taskResult.getTimestamp("updated_at");
            String statusValue = taskResult.getString("status");
            TaskStatus status = TaskStatus.fromValue(statusValue);
            tasks.add(new AllTaskResultDTO(id,user_id, title, desc, created_at, updated_at, status));
        }
        return tasks;
    }

    @Override
    public boolean deleteTaskByID(int id,int user_id) throws SQLException {
        Connection connection = null;
        connection = JDBCConnection.getConnection();
        connection.setAutoCommit(false);
        String sqlDelete = """
                delete from tasks where  id = ? and user_id = ?
                """;
        PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
        preparedStatement.setInt(1, id);
        preparedStatement.setInt(2, user_id);
        preparedStatement.execute();
        connection.commit();
        return true;
    }

    @Override
    public AllTaskResultDTO createTask(TaskResultDTO taskResultDTO) throws SQLException {
        Connection connection = null;
        connection = JDBCConnection.getConnection();
        connection.setAutoCommit(false);
        String sqlInsert = """
                insert into tasks(user_id, title, status, "desc", created_at, updated_at)
                 values (?,?,?::task_status,?,?,?) RETURNING id; 
                """;
        PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
        preparedStatement.setInt(1, taskResultDTO.user_id());
        preparedStatement.setString(2, taskResultDTO.title());
        preparedStatement.setString(3, taskResultDTO.status().getValue());
        preparedStatement.setString(4, taskResultDTO.desc());
        preparedStatement.setTimestamp(5, taskResultDTO.created_at());
        preparedStatement.setTimestamp(6, taskResultDTO.updated_at());

        ResultSet rs =  preparedStatement.executeQuery();
        connection.commit();
        if (rs.next()) {
            int insertedId = rs.getInt("id");
            return new AllTaskResultDTO(insertedId,taskResultDTO.user_id(),taskResultDTO.title(),taskResultDTO.desc(),taskResultDTO.created_at(),taskResultDTO.updated_at(),taskResultDTO.status());
        }
        return null;
    }

    @Override
    public AllTaskResultDTO updateTask(TaskUpdatetDTO taskUpdatetDTO) throws SQLException {
        Connection connection = null;
        connection = JDBCConnection.getConnection();
        connection.setAutoCommit(false);
        String sqlInsert = """
                 update  tasks set title = ?,"desc"=?,status=?::task_status,updated_at=? where id=? and user_id=?
                RETURNING *; 
                """;
        PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
        preparedStatement.setString(1, taskUpdatetDTO.title());
        preparedStatement.setString(2, taskUpdatetDTO.desc());
        preparedStatement.setString(3, taskUpdatetDTO.status().getValue());
        preparedStatement.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));
        preparedStatement.setInt(5, taskUpdatetDTO.id());
        preparedStatement.setInt(6, taskUpdatetDTO.user_id());
        ResultSet rs =  preparedStatement.executeQuery();
        connection.commit();
        AllTaskResultDTO allTaskResultDTO =null;
        if (rs.next()) {
            int id = rs.getInt("id");
            int user_id = rs.getInt("user_id");
            String title = rs.getString("title");
            String desc = rs.getString("desc");
            TaskStatus status = rs.getObject("status",TaskStatus.class);
            Timestamp created_at = rs.getTimestamp("created_at");
            Timestamp updated_at = rs.getTimestamp("updated_at");
            allTaskResultDTO = new AllTaskResultDTO(id,user_id,title,desc,created_at,updated_at,status);
        }

        return allTaskResultDTO;
    }
}
