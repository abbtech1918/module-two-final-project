package org.abbtech.moduleTwoTask.models;

import org.abbtech.moduleTwoTask.enums.TaskStatus;

import java.sql.Timestamp;


public class Task {
    private int id;
    private int user_id;
    private String title;
    private String desc;
    private TaskStatus status;
    private Timestamp created_at;
    private Timestamp updated_at;
}
