package org.abbtech.moduleTwoTask.servlet.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.abbtech.moduleTwoTask.ApiResponse;
import org.abbtech.moduleTwoTask.repositores.UserRepository;
import org.abbtech.moduleTwoTask.utils.SecurityUtil;

import java.io.IOException;

@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/user/tasks")
public class AuthenticationFilter implements Filter {
    private ApiResponse<Object> apiResponse;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
        apiResponse = new ApiResponse<>("Authentication successful!","success",null);
    }
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        Cookie userCookie = SecurityUtil.getCookieByName(req.getCookies(), "username");
        HttpSession httpSessionUsername = SecurityUtil.getSessionByAttributeName(req.getSession(false), "username");
        HttpSession httpSessionId = SecurityUtil.getSessionByAttributeName(req.getSession(false), "id");
        resp.setContentType("application/json");
        if (userCookie != null && httpSessionUsername != null && httpSessionId != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            resp.setStatus(403);
            apiResponse.setStatus("false");
            apiResponse.setMessage("You do not have permission to access this resource.");
            apiResponse.setData(null);
            resp.getWriter().write(apiResponse.toJson());
        }
    }
}