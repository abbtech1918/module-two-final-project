package org.abbtech.moduleTwoTask.servlet.routes;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.abbtech.moduleTwoTask.ApiResponse;
import org.abbtech.moduleTwoTask.dtos.UserResultDTO;
import org.abbtech.moduleTwoTask.repositores.UserRepository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

@WebServlet(name = "UserLoginServlet", urlPatterns = "/user-login")
public class UserLoginServlet extends HttpServlet {
    private UserRepository userRepository;
    private ApiResponse<Object> apiResponse;
    @Override
    public void init() throws ServletException {
        super.init();
        userRepository = new UserRepository();
        apiResponse = new ApiResponse<>("Authentication successful!","success",null);
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();
        resp.setContentType("application/json");
        String actualUsername = req.getParameter("user");
        String actualPass = req.getParameter("pass");
        UserResultDTO user = null;
        try {
             user =userRepository.getOneUser(actualUsername,actualPass);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (Objects.nonNull(user)) {
            HttpSession httpSession = req.getSession();
            httpSession.setAttribute("id", user.id());
            httpSession.setAttribute("username", user.username());
            Cookie cookieUser = new Cookie("username", user.username());
            Cookie cookieUserId = new Cookie("id", String.valueOf(user.id()));
            cookieUser.setMaxAge(300);
            cookieUserId.setMaxAge(300);
            resp.addCookie(cookieUser);
            resp.addCookie(cookieUserId);
            apiResponse.setStatus("true");
            apiResponse.setMessage("Authentication successful!");
            apiResponse.setData(actualUsername+":"+actualPass);
            writer.write(apiResponse.toJson());
        }
        else{
            resp.setStatus(401);
            apiResponse.setStatus("false");
            apiResponse.setMessage("Username or password incorrect");
            apiResponse.setData(actualUsername+" : "+actualPass);
            writer.write(apiResponse.toJson());
        }

    }
}

