package org.abbtech.moduleTwoTask.servlet.routes;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.abbtech.moduleTwoTask.ApiResponse;
import org.abbtech.moduleTwoTask.dtos.AllTaskResultDTO;
import org.abbtech.moduleTwoTask.dtos.TaskResultDTO;
import org.abbtech.moduleTwoTask.dtos.TaskUpdatetDTO;
import org.abbtech.moduleTwoTask.enums.TaskStatus;
import org.abbtech.moduleTwoTask.repositores.TaskRepository;
import org.abbtech.moduleTwoTask.repositores.UserRepository;
import org.abbtech.moduleTwoTask.utils.SecurityUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet(name = "TaskServlet", urlPatterns = "/user/tasks")
public class TaskServlet extends HttpServlet {
    private UserRepository userRepository;
    private TaskRepository taskRepository;
    private ApiResponse<Object> apiResponse;

    @Override
    public void init() throws ServletException {
        super.init();
        userRepository = new UserRepository();
        taskRepository = new TaskRepository();
        apiResponse = new ApiResponse<>("null", "false", null);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();
        resp.setContentType("application/json");
        HttpSession httpSessionUserId = SecurityUtil.getSessionByAttributeName(req.getSession(false), "id");
        TaskResultDTO taskResultDTO = null;
        int user_id = Integer.parseInt(httpSessionUserId.getAttribute("id").toString());
        int task_id = Integer.parseInt(req.getParameter("task_id"));
        try {
            if(taskRepository.deleteTaskByID(task_id,user_id)){
                resp.setStatus(200);
                apiResponse.setStatus("true");
                apiResponse.setMessage("Task deleted successfully");
                apiResponse.setData(taskResultDTO);
                writer.write(apiResponse.toJson());
            }
            else {
                resp.setStatus(404);
                apiResponse.setStatus("false");
                apiResponse.setMessage("Something went wrong");
                writer.write(apiResponse.toJson());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();
        resp.setContentType("application/json");
        HttpSession httpSessionUserId = SecurityUtil.getSessionByAttributeName(req.getSession(false), "id");
        AllTaskResultDTO taskResultDTO = null;
        int user_id = Integer.parseInt(httpSessionUserId.getAttribute("id").toString());
        String title = req.getParameter("title");
        String desc = req.getParameter("desc");
        TaskStatus status = TaskStatus.PROCESSING; // Example enum value
        Timestamp created_at = Timestamp.valueOf(LocalDateTime.now());
        Timestamp updated_at = Timestamp.valueOf(LocalDateTime.now());
        try {
            taskResultDTO = taskRepository.createTask(new TaskResultDTO(user_id, title, desc, created_at, updated_at, status));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        resp.setStatus(201);
        apiResponse.setStatus("true");
        apiResponse.setMessage("Task created successfully");
        apiResponse.setData(taskResultDTO);
        writer.write(apiResponse.toJson());
    }
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();
        resp.setContentType("application/json");
        HttpSession httpSessionUserId = SecurityUtil.getSessionByAttributeName(req.getSession(false), "id");
        AllTaskResultDTO allTaskResultDTO = null;
        int user_id = Integer.parseInt(httpSessionUserId.getAttribute("id").toString());
        int task_id = Integer.parseInt(req.getParameter("task_id"));
        String title = req.getParameter("title");
        String desc = req.getParameter("desc");
        TaskStatus status = TaskStatus.fromValue(req.getParameter("status"));
        try {
           allTaskResultDTO = taskRepository.updateTask(new TaskUpdatetDTO(task_id,user_id,title,desc,status));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if(Objects.nonNull(allTaskResultDTO)){
            resp.setStatus(200);
            apiResponse.setStatus("true");
            apiResponse.setMessage("Task updated successfully");
            apiResponse.setData(allTaskResultDTO);
            writer.write(apiResponse.toJson());
        }
        else{
            resp.setStatus(400);
            apiResponse.setStatus("false");
            apiResponse.setMessage("Something Went Wrong");
            apiResponse.setData(allTaskResultDTO);
            writer.write(apiResponse.toJson());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();
        resp.setContentType("application/json");
        HttpSession httpSessionUserId = SecurityUtil.getSessionByAttributeName(req.getSession(false), "id");
        List<AllTaskResultDTO> taskResultDTOS = new ArrayList<>();
        try {
            taskResultDTOS = taskRepository.getUserTasks(Integer.parseInt(httpSessionUserId.getAttribute("id").toString()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (!taskResultDTOS.isEmpty()) {
            apiResponse.setStatus("true");
            apiResponse.setMessage("Tasks fetched successfully");
            apiResponse.setData(taskResultDTOS);
            writer.write(apiResponse.toJson());
        } else {
            resp.setStatus(404);
            apiResponse.setStatus("false");
            apiResponse.setMessage("Not found any tasks");
            apiResponse.setData(null);
            writer.write(apiResponse.toJson());
        }
    }
}

