package org.abbtech.moduleTwoTask.interfaces;

import org.abbtech.moduleTwoTask.dtos.UserResultDTO;
import org.abbtech.moduleTwoTask.models.User;

import java.sql.SQLException;

public interface IUserRepository {
    boolean checkUserExistOrNot(String username,String password) throws SQLException;
    UserResultDTO getOneUser(String username,String password) throws SQLException;
}
