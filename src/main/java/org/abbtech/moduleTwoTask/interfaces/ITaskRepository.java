package org.abbtech.moduleTwoTask.interfaces;

import org.abbtech.moduleTwoTask.JDBCConnection;
import org.abbtech.moduleTwoTask.dtos.AllTaskResultDTO;
import org.abbtech.moduleTwoTask.dtos.TaskResultDTO;
import org.abbtech.moduleTwoTask.dtos.TaskUpdatetDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {
    List<AllTaskResultDTO> getUserTasks(int user_id) throws SQLException;

    boolean deleteTaskByID(int id, int user_id) throws SQLException;

    AllTaskResultDTO createTask(TaskResultDTO taskResultDTO) throws SQLException;
    AllTaskResultDTO updateTask(TaskUpdatetDTO taskUpdatetDTO) throws SQLException;
}
