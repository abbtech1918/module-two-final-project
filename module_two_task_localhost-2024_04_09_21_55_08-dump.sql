--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: task_status; Type: TYPE; Schema: public; Owner: abbtech
--

CREATE TYPE public.task_status AS ENUM (
    'processing',
    'completed',
    'deleted'
);


ALTER TYPE public.task_status OWNER TO abbtech;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.tasks (
                              id integer NOT NULL,
                              user_id integer,
                              title character varying(255) NOT NULL,
                              "desc" character varying(255) NOT NULL,
                              created_at timestamp(6) without time zone DEFAULT now() NOT NULL,
                              updated_at timestamp(6) without time zone DEFAULT now() NOT NULL,
                              status public.task_status DEFAULT 'processing'::public.task_status NOT NULL
);


ALTER TABLE public.tasks OWNER TO abbtech;

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.tasks_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO abbtech;

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: abbtech
--

CREATE TABLE public.users (
                              id integer NOT NULL,
                              username character varying(255) NOT NULL,
                              password character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO abbtech;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: abbtech
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO abbtech;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: abbtech
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: abbtech
--

INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (1, 1, 'test', 'test', '2024-04-09 20:20:02', '2024-04-09 20:20:11', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (2, 1, 'Test', 'processing', '2024-04-09 22:52:05.615987', '2024-04-09 22:52:05.615987', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (4, 2, 'Test Title', 'Test Desc', '2024-04-10 03:14:26.753592', '2024-04-10 03:14:26.753592', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (5, 2, 'Test Title', 'Test Desc', '2024-04-10 03:18:09.798912', '2024-04-10 03:18:09.798912', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (6, 2, 'Test Title', 'Test Desc', '2024-04-10 03:23:21.344013', '2024-04-10 03:23:21.34502', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (7, 2, 'Test Title', 'Test Desc', '2024-04-10 03:23:22.345405', '2024-04-10 03:23:22.345405', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (8, 2, 'Test Title', 'Test Desc', '2024-04-10 03:23:23.213349', '2024-04-10 03:23:23.213349', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (9, 2, 'Test Title', 'Test Desc', '2024-04-10 03:33:49.490381', '2024-04-10 03:33:49.490381', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (10, 2, 'Test Title', 'Test Desc', '2024-04-10 03:40:56.047622', '2024-04-10 03:40:56.047622', 'processing');
INSERT INTO public.tasks (id, user_id, title, "desc", created_at, updated_at, status) VALUES (11, 2, 'Updated Test Title', 'Updated Test Title', '2024-04-10 03:41:01.060721', '2024-04-10 00:45:20.336462', 'completed');


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: abbtech
--

INSERT INTO public.users (id, username, password) VALUES (1, 'username', '1a1dc91c907325c69271ddf0c944bc72');
INSERT INTO public.users (id, username, password) VALUES (2, 'admin', '1a1dc91c907325c69271ddf0c944bc72');


--
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.tasks_id_seq', 11, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- Name: tasks tasks_pk; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pk PRIMARY KEY (id);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);


--
-- Name: users users_pk2; Type: CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk2 UNIQUE (username);


--
-- Name: tasks tasks_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: abbtech
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_users_id_fk FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

